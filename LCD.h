#pragma once
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <string.h>
class LCD
{
public:
	LCD();
	LCD(char * STRING);
	~LCD();
	char * createstring();
	int routenum;
	bool skipstop;
	bool requeststop;
	bool manual;
	bool announce;
};
