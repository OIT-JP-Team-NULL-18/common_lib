# Data_Objects

All of the object definitions needed for our Projects

LED: stop name, route number\
LCD: Route number, Stop name, Stop name... //number of stops to send is in settings.conf\
Audio: File Name, Stop Name //one will always be null file name for prerecorded audio, stop name to use tts

settings.conf\
	Number of stops to send to LCD\
	Inner threshold % //% of theshold to change to next stop \
	minimun inner threshold //absolute minimum inner theshold distance\
	SocketLED\
	SocketLCD\
	SocketAudio\
	SocketGPS \
	SocketBell\