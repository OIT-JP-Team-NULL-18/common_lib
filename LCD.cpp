	#include "LCD.h"

	/*	bool requeststop;
	bool manual;
	bool skipstop;
	int routenum;
	*/
	LCD::LCD(): manual(false), skipstop(false), requeststop(false),routenum(-1),announce(false)
	{

	}
	LCD::LCD(char * STRING): manual(false), skipstop(false), requeststop(false),routenum(-1),announce(false)
	{
		char * num = strtok(STRING, ":");
		if(num != nullptr)
		{
			routenum = atoi(num);
			char * skip = strtok(nullptr, ":");
			if(skip != nullptr)
			{
				int skipi = atoi(skip);
				if(skipi == 0) skipstop = false;
				else skipstop = true;

				char * request = strtok(nullptr, ":");
				if(request != nullptr)
				{
					int requesti = atoi(request);
					if(requesti == 0) requeststop = false;
					else requeststop = true;

					char * man = strtok(nullptr, ":");
					if(man != nullptr)
					{
						int mani = atoi(man);
						if(mani == 0) manual = false;
						else manual = true;

						char * anoun = strtok(nullptr, ":");
						if(anoun != nullptr)
						{
							int anouni = atoi(anoun);
							if(anouni == 0) announce = false;
							else announce = true;
						}
					}
				}
			}

		}
	}
	LCD::~LCD()
	{

	}
	/*
		int routenum;
	bool skipstop;
	bool requeststop;
	bool manual;
	*/
	char * LCD::createstring()//routenum:skipstop:requeststop:manual:
	{
		char tempbuffer[25];
		sprintf(tempbuffer,"%d:%d:%d:%d:%d:", routenum,skipstop,requeststop, manual,announce);
		char * retVal = new char[strlen(tempbuffer) + 1];
		strcpy(retVal, tempbuffer);
		return retVal;
	}
