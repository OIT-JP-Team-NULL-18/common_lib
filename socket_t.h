#ifndef _SOCKET_T_H
#define _SOCKET_T_H
#include <openssl/sha.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <string.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>


#include "Exception.h"
enum socket_type_t{
  SERVER,
  CLIENT,
  UNCONFIGURED
};
class socket_t
{
public:
  socket_t();
  ~socket_t();
  socket_t(const socket_t & copy) = delete;
  socket_t & operator=(const socket_t & rhs) = delete;

  bool data_avail();
  void setup_server(const char * name);
  void setup_client(const char * name);
  bool server_accept(int timeout);
  int send_msg(void * msg, int len);
  void * receive_msg(int size);
  bool connected(); 
private:
  bool check_msg(void * msg, unsigned char hash[SHA_DIGEST_LENGTH]);
  unsigned char * generate_hash(void * msg);
  struct sockaddr_un m_addr;
  char * m_socket_name;
  int m_socket_fd;
  int m_accept_fd;
  socket_type_t m_socket_type;
};
#endif
