#include "socket_t.h"
#include "Stop.hpp"
socket_t::socket_t() :
    m_socket_name(nullptr),
    m_socket_fd(-1),
    m_accept_fd(-1)
{
}

socket_t::~socket_t()
{
    delete[] m_socket_name;
    m_socket_fd = -1;
}

bool socket_t::server_accept(int timeout)
{
	fd_set rfds;
	struct timeval tv;
	int retval;

	/* Watch stdin (fd 0) to see when it has input. */

	FD_ZERO(&rfds);
	FD_SET(m_socket_fd, &rfds);

	/* Wait up to five seconds. */

	tv.tv_sec = timeout;
	tv.tv_usec = 0;

	retval = select(m_socket_fd + 1, &rfds, NULL, NULL, &tv);
	/* Don't rely on the value of tv now! */

	if (retval == -1)
		perror("select()");
	else if (retval)
	{
		printf("Data is available now.\n");
		m_accept_fd = accept(m_socket_fd, NULL, NULL);
	}
	/* FD_ISSET(0, &rfds) will be true. */
	else
		printf("No data within timelimit.\n");

    

    return m_accept_fd == 0;
}

void socket_t::setup_server(const char *name)
{
    m_socket_name = new char [strlen(name)+ strlen("/usr/share/") + 1];
    strcpy(m_socket_name, "/usr/share/");
    strcat(m_socket_name, name);
    m_socket_type = SERVER;
    m_socket_fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(m_socket_fd == -1)
    {
        throw(Exception("Wrong File Decripter", false, __LINE__, __FILE__));
    }
    //fcntl(m_socket_fd, )
    memset(&m_addr, 0, sizeof(m_addr));
    m_addr.sun_family = AF_UNIX;
    strncpy(m_addr.sun_path, m_socket_name, sizeof(m_addr.sun_path) - 1);
    unlink(m_socket_name);
    if (bind(m_socket_fd, (struct sockaddr *) &m_addr, sizeof(m_addr)) == -1)
    {
        throw(Exception("Bind didn't work", false, __LINE__, __FILE__));
    }
    if (listen(m_socket_fd, 5) == -1)
    {
        throw(Exception("Listen didn't work", false, __LINE__, __FILE__));
    }
}

void socket_t::setup_client(const char *name)
{
    m_socket_name = new char [strlen(name)+ strlen("/usr/share/") + 1];
    strcpy(m_socket_name, "/usr/share/");
    strcat(m_socket_name, name);
    m_socket_type = CLIENT;
    m_socket_fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(m_socket_fd == -1)
    {
        throw(Exception("Wrong fd"));
    }
    memset(&m_addr, 0, sizeof(m_addr));
    m_addr.sun_family = AF_UNIX;
    strcpy(m_addr.sun_path, m_socket_name);
    while (connect(m_socket_fd, (const sockaddr *)&m_addr, sizeof(m_addr)) == -1)
    {
	printf("Awaiting Connection\n");
	fflush(stdout);
	sleep(2);
    }
}

bool socket_t::check_msg(void * msg, unsigned char hash[SHA_DIGEST_LENGTH])
{
    unsigned char new_hash[SHA_DIGEST_LENGTH];
    /*SHA1(msg, new_hash);
    if(strcmp(hash, new_hash) != 0)
    {
        return false;
    }*/
    return true;
}

int socket_t::send_msg(void * msg, int len)
{
    int n = -1;
    if (m_accept_fd != -1)
        n = write(m_accept_fd, msg, len);
    else
        n = write(m_socket_fd, msg, len);
    return n;
}

void * socket_t::receive_msg(int size)
{
    char * rd_buff = new char[size];
    int n = -1;
    if (m_accept_fd != -1)
        n = read(m_accept_fd, rd_buff, sizeof(char) * size);
    else
        n = read(m_socket_fd, rd_buff, sizeof(char) * size);
    if (n < 0)
    {
        throw(Exception("Error Reading"));
    }
    return (void *)rd_buff;
}

bool socket_t::data_avail()
{
    fd_set rfds;
    struct timeval tv;
    int retval;
    int fdtosrch = -1;

    if(m_socket_type == CLIENT)
    {
	fdtosrch = m_socket_fd;
    }
    else
    {
	fdtosrch = m_accept_fd;
    }    
    FD_ZERO(&rfds);
    FD_SET(fdtosrch, &rfds);
    if(!FD_ISSET(fdtosrch,&rfds))
    {
	throw(Exception("File Descriptor not in  set"));
    }
    tv.tv_sec = 0;
    tv.tv_usec = 250;
    retval = select(fdtosrch + 1, &rfds, NULL, NULL, &tv);
    if (retval == -1)
	perror("select()");
    else if (retval)
    {
	return true;
    }			
    return false;
}

bool socket_t::connected()
{
	bool connected = false;
	if(m_socket_type == CLIENT)
    {
		if(m_socket_fd != -1) connected = true;
    }
    else
    {
		if(m_accept_fd != -1) connected = true;
    }    
	return connected;
}
