#include "Exception.h"
Exception::Exception(const char         *msg,
            bool          recoverable,
            unsigned int linenum,
            const char * file):
            m_recoverable(recoverable),
            m_linenum(linenum)
{
    m_msg = new char[strlen(msg) + 1];
    strcpy(m_msg, msg);
    m_file = new char [strlen(file) + 1];
    strcpy(m_file, m_file);
}
Exception::Exception(const char * msg) :
    m_file(nullptr),
    m_recoverable(false),
    m_linenum(-1)
{
    m_msg = new char[strlen(msg) + 1];
    strcpy(m_msg, msg);
}

char * Exception::GetString()
{
    return m_msg;
}
