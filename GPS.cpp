#include "GPS.h"


GPS::GPS()
{
}

GPS::GPS(char* mystring)
{
	char * buffer = strdup(mystring);
	char * lat = strtok(buffer, ",");
	if(lat != nullptr)
	{ 
		char* lon = strtok(NULL, "\n,");
		Location.Latitude = atof(lat);
		Location.Longitude = atof(lon);
		free(buffer);
		buffer = nullptr;
	}
}

GPS::~GPS()
{
}

void GPS::parseNEMAstring(char* NEMA)
{
	char latbuffer[4] = {0};
	char lonbuffer[4] = {0};
	char latbuffer2[9] = {0};
	char lonbuffer2[9] = {0};
	char * lat = nullptr;
	char * latdir = nullptr;
	char * lon = nullptr;
	char * londir = nullptr;
	char* latleft = nullptr;
	char* latright = nullptr;
	char* lonleft = nullptr;
	char* lonright = nullptr;
	int latlen = 0;
	int lonlen = 0;
	double latdeg = 0;
	double latmin = 0;
	double londeg = 0;
	double lonmin = 0;
	double ddlat = 0;
	double ddlon = 0;
	
	char * garbage = strtok(NEMA,",");
	if(garbage != nullptr) garbage = strtok(NULL,",");
	if(garbage != nullptr) garbage = strtok(NULL,",");
	if(garbage != nullptr) lat = strtok(NULL,",");
	if(lat != nullptr) latdir = strtok(NULL,",");
	if(latdir != nullptr)lon = strtok(NULL,",");
	if(lon != nullptr)londir = strtok(NULL,",");
	
	if(londir != nullptr)
	{
	latleft = strtok(lat,".");
	latright = strtok(NULL,".");
	
	lonleft = strtok(lon,".");
	lonright = strtok(NULL,".");
	if(latleft != nullptr && lonleft != nullptr)
	{
		latlen = strlen(latleft);
		lonlen = strlen(lonleft);
	
		for(int i = 0; i < latlen - 2; i++)
		{
			latbuffer[i] = latleft[i];
		}
	
		latbuffer2[0] = latleft[latlen - 2];
		latbuffer2[1] = latleft[latlen - 1];
		latbuffer2[2] = '.';
		for(int i = 0; i < 5; i++)
		{
			latbuffer2[i+3] = latright[i];
		}
		for(int i = 0; i < lonlen - 2; i++)
		{
			lonbuffer[i] = lonleft[i];
		}
	
		lonbuffer2[0] = lonleft[lonlen - 2];
		lonbuffer2[1] = lonleft[lonlen - 1];
		lonbuffer2[2] = '.';
		for(int i = 0; i < 5; i++)
		{
			lonbuffer2[i+3] = lonright[i];
		}
		latdeg = atof(latbuffer);
		latmin = atof(latbuffer2);
		londeg = atof(lonbuffer);
		lonmin = atof(lonbuffer2);
		ddlat = latdeg + (latmin / 60);
		ddlon = londeg + (lonmin / 60);
		if(latdir[0] == 'S')
		{
			ddlat = ddlat * -1;
		}
		if(londir[0] == 'W')
		{
			ddlon = ddlon * -1;
		}
		Location.Latitude = ddlat;
		Location.Longitude = ddlon;
	}
	}
	else
	{
		Location.Latitude = 0;
		Location.Longitude = 0;
	}
}

char* GPS::createstring()
{
	char lat[100];
    char lon[100];
    sprintf(lat, "%.8f", Location.Latitude);
    sprintf(lon, "%.8f", Location.Longitude);

    string buffer = string(lat) + ',' +string(lon) + ',' + '\n' + '\0';
    char * retVal = new char[buffer.length() + 1];
    strcpy(retVal, buffer.c_str());
    return retVal;
}
