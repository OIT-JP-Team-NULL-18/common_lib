#include "Stop.hpp"
Stop::Stop(): stopthreshold(150)
{
}
Stop::Stop(char * StopInfo): stopthreshold(50)
{
    char * temp = nullptr;
    if(!StopInfo)
    {
        throw(Exception("Invalid"));
    }

    Name = strtok(StopInfo, ",");
    if((temp = strtok(nullptr, ",")) != nullptr)
    {
        StopID = temp;
    }
    if((temp = strtok(nullptr, ",")) != nullptr)
    {
        gps.Location.Latitude = atof(temp);
    }
    if((temp = strtok(nullptr, ",")) != nullptr)
    {
        gps.Location.Longitude = atof(temp);
    }
    if((temp = strtok(nullptr, ",")) != nullptr)
    {
        Voice = temp;
    }
    //if((temp = strtok(nullptr, ",")) != nullptr)
    //{
    //    stopthreshold = atoi(temp);
    //}
}

char * Stop::CreateString()
{
    char * mygps = gps.createstring();
    char threshold[100];
    
    sprintf(threshold, "%d", stopthreshold);
    string buffer = Name +','+ StopID + ',' + mygps + ',' + Voice + ',' + threshold;
    char * retVal = new char[buffer.length() + 1];
    strcpy(retVal, buffer.c_str());
    delete mygps;
    return retVal;
}
