#define _CRT_SECURE_NO_WARNINGS
#define BUFF_SIZE 1024
#include <algorithm>
#include "settings.h"
using std::getline;
using std::string;
using std::ifstream;
settings::settings() :
    m_lcd_socket("lcd"),
    m_gps_socket("gps"),
    m_led_socket("led"),
    m_bell_socket("bell"),
    m_lcd_stop_num(1),
    m_threshold_min(100),
    m_threshold_percent(40)
{
	string temp;
	char buffer[BUFF_SIZE];
    string option;
    string setting;
    ifstream settings_file("/usr/share/settings.conf");
    if(settings_file.good())
    {
        while(getline(settings_file,temp))
        {
			strcpy(buffer, temp.c_str());
			if (buffer[0] != '#')
            {
                setting = strtok(buffer, ": ");
                option = strtok(nullptr, ": \n");
                if(setting == "LED_SOCKET")
                {
                    m_led_socket = option;
                }
                else if(setting == "LCD_SOCKET")
                {
                    m_lcd_socket = option;
                }
                else if(setting == "BELL_SOCKET")
                {
                    m_bell_socket = option;
                }
                else if(setting == "GPS_SOCKET")
                {
                    m_gps_socket = option;
                }
                else if(setting == "LCD_STOP_NUM")
                {
                    m_lcd_stop_num = atoi(option.c_str());
                }
                else if(setting == "THRESHOLD_MIN")
                {
                    m_threshold_min = atoi(option.c_str());
                }
                else if("THRESHOLD_PERCENT")
                {
                    m_threshold_percent = atoi(option.c_str());
                }
            }
        }
    }
}

settings::~settings()
{
}

std::string settings::get_lcd_socket() const
{
	return m_lcd_socket;
}

std::string settings::get_led_socket() const
{
	return m_led_socket;
}

std::string settings::get_gps_socket() const
{
	return m_gps_socket;
}

std::string settings::get_bell_socket() const
{
	return m_bell_socket;
}

int settings::get_threshold_percent() const
{
	return m_threshold_percent;
}

int settings::get_threshold_min() const
{
	return m_threshold_min;
}

int settings::get_lcd_stop_num() const
{
	return m_lcd_stop_num;
}
