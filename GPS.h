#pragma once
#include "Coordinate.h"
#include <string>
#include <string.h>
using std::string;
class GPS
{
public:
	GPS();
	GPS(char*);
	~GPS();
	void parseNEMAstring(char* NEMA);
	///any additional information needed; for testing purposes just lat lon
	Coordinate Location;
	char* createstring();

};


