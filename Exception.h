#ifndef _EXCEPTION_H
#define _EXCEPTION_H
#include <string.h>
class Exception{
public:
  Exception(const char         *msg,
            bool          recoverable,
            unsigned int           linenum,
            const char * file);
  Exception(const char * msg);
  char * GetString();
private:
  char *m_file;
  unsigned int m_linenum;
  bool m_recoverable;
  char * m_msg;
};
#endif
