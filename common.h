#ifndef _COMMON_H
#define _COMMON_H
#include "Coordinate.h"
#include "GPS.h"
#include "Stop.hpp"
#include "settings.h"
#include "socket_t.h"
#include "Bellcord.h"
#include "LCD.h"

#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 480

#define BUS_STOP_ANNOUNCEMENT_SYSTEM_VERSION "0.1"

#endif
