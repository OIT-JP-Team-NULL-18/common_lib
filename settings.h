#ifndef _SETTINGS_H
#define _SETTINGS_H
#include <fstream>
#include <string>
#include <string.h>
class settings
{
public:
  settings();
  //settings(char * settings_file);
  ~settings();
  /*Sockets*/
  std::string get_lcd_socket() const;
  std::string get_led_socket() const;
  std::string get_gps_socket() const;
  std::string get_bell_socket() const;
  /*Threshold Information*/
  int get_threshold_percent() const;
  int get_threshold_min() const;
  /*LCD Settings*/
  int get_lcd_stop_num() const;

private:
  std::string m_lcd_socket;
  std::string m_led_socket;
  std::string m_gps_socket;
  std::string m_bell_socket;

  int m_threshold_percent;
  int m_threshold_min;

  int m_lcd_stop_num;
};
#endif
